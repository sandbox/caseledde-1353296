<?php

/**
 * @file
 * Add operator to provide 'RLIKE' with regular expressions.
 */

/**
 * Extends the class views_handler_filter_string to provide regular expressions.
 */
class views_regex_filter_handler_filter_string extends views_handler_filter_string {

  function operators() {
    $operators = parent::operators();
    $operators += array(
      'reg' => array(
        'title' => t('Regular expression'),
        'short' => t('regex'),
        'method' => 'op_regex',
        'values' => 1,
      ),
    );
    return $operators;
  }

  function operator() {
    return 'RLIKE';
  }

  function op_regex($field) {
    $this->query->add_where($this->options['group'], $field, $this->value, $this->operator());
  }

}
