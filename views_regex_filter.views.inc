<?php

/**
 * @file
 * Provide views data and handlers for views_regex_filter.module
 */

/**
 * Implements hook_views_data().
 */
function views_regex_filter_views_data_alter(&$data) {
  $data = replace_views_handler_filter_string($data);
}

/**
 * Helper function to replace views_handler_filter_string by 
 * views_regex_filter_handler_filter_string.
 */
function replace_views_handler_filter_string($array) {
  foreach ($array as $key => $value) {
    if (is_array($value)) {
      $array[$key] = replace_views_handler_filter_string($value);
    }
    else if ($key === 'handler' && $value === 'views_handler_filter_string') {
      $array[$key] = 'views_regex_filter_handler_filter_string';
    }
  }
  return $array;
}